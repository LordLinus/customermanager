package de.academy.CustomerManager.Mitarbeiter;

import de.academy.CustomerManager.Unternehmen.Unternehmen;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.List;

@Entity
public class Mitarbeiter {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    @ManyToOne
    Unternehmen unternehmen;

    public Mitarbeiter() {
    }

    public Mitarbeiter(String name, Unternehmen unternehmen) {
        this.name = name;
        this.unternehmen = unternehmen;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Unternehmen getUnternehmen() {
        return unternehmen;
    }
}
