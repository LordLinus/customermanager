package de.academy.CustomerManager.Mitarbeiter;

public class MitarbeiterDTO {
    private String name;

    public MitarbeiterDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
