package de.academy.CustomerManager.Mitarbeiter;

import org.springframework.data.repository.CrudRepository;

public interface MitarbeiterRepository extends CrudRepository<Mitarbeiter, Integer> {
}
