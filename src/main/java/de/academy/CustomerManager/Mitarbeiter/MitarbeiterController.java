package de.academy.CustomerManager.Mitarbeiter;

import de.academy.CustomerManager.Unternehmen.UnternehmenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MitarbeiterController {
    private MitarbeiterService mitarbeiterService;
    private UnternehmenService unternehmenService;

    @Autowired
    public MitarbeiterController(MitarbeiterService mitarbeiterService, UnternehmenService unternehmenService) {
        this.mitarbeiterService = mitarbeiterService;
        this.unternehmenService = unternehmenService;
    }

    @GetMapping("/MitarbeiterIndex")
    public String mittarbeiterIndex(Model model, @ModelAttribute(value = "unternehmenId") Integer unternehmenId) {
        model.addAttribute("unternehmen", unternehmenService.getById(unternehmenId));
        model.addAttribute("mitarbeiterDTO", new MitarbeiterDTO(""));
        return "/mitarbeiter/MitarbeiterIndex";
    }
    @PostMapping("/MitarbeiterIndex")
    public String addMitarbeiter(@ModelAttribute(value = "mitarbeiterDTO") MitarbeiterDTO mitarbeiterDTO,
                                 @ModelAttribute(value = "unternehmenId") Integer unternehmenId){
        mitarbeiterService.addMitarbeiter(mitarbeiterDTO, unternehmenService.getById(unternehmenId));
        return "redirect:/MitarbeiterIndex?unternehmenId=" + unternehmenId;
    }

    @PostMapping("/MitarbeiterDelete")
    public String deleteMitarbeiter(@ModelAttribute(value = "mitarbeiterId") Integer mitarbeiterId){
        mitarbeiterService.deleteMitarbeiter(mitarbeiterId);
        return "redirect:/unternehmen";
    }
    @GetMapping("/MitarbeiterEdit")
    public String editMitarbeiter(Model model,@ModelAttribute(value = "mitarbeiterId") Integer mitarbeiterId){
        Mitarbeiter mitarbeiter = mitarbeiterService.getById(mitarbeiterId);
        model.addAttribute("mitarbeiterDTO", new MitarbeiterDTO(mitarbeiter.getName()));
        model.addAttribute("mitarbeiterId",mitarbeiterId);
        return "/mitarbeiter/MitarbeiterEdit";
    }

    @PostMapping("/MitarbeiterEdit")
    public String editMitarbeiterForRealTho(@ModelAttribute(value = "mitarbeiterId") Integer mitarbeiterId,
                                            @ModelAttribute(value = "mitarbeiterDTO") MitarbeiterDTO mitarbeiterDTO){
        mitarbeiterService.editMitarbeiter(mitarbeiterId,mitarbeiterDTO);
        return "redirect:/unternehmen";
    }
}
