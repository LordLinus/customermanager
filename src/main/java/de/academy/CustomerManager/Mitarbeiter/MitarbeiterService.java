package de.academy.CustomerManager.Mitarbeiter;

import de.academy.CustomerManager.Unternehmen.Unternehmen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MitarbeiterService {
    private MitarbeiterRepository mitarbeiterRepository;

    @Autowired
    public MitarbeiterService(MitarbeiterRepository mitarbeiterRepository) {
        this.mitarbeiterRepository = mitarbeiterRepository;
    }

    public Mitarbeiter getById(int id){
        return mitarbeiterRepository.findById(id).orElse(null);
    }


    public void addMitarbeiter(MitarbeiterDTO mitarbeiterDTO, Unternehmen unternehmen){
        Mitarbeiter mitarbeiter = new Mitarbeiter(mitarbeiterDTO.getName(), unternehmen);
        mitarbeiterRepository.save(mitarbeiter);
    }
    public void deleteMitarbeiter(int mitarbeiterId){
        Optional<Mitarbeiter> optionalMitarbeiter = mitarbeiterRepository.findById(mitarbeiterId);
        if (optionalMitarbeiter.isPresent()){
            mitarbeiterRepository.delete(optionalMitarbeiter.get());
        }
    }
    public void editMitarbeiter(int mitarbeiterId, MitarbeiterDTO mitarbeiterDTO){
        Optional<Mitarbeiter> optionalMitarbeiter = mitarbeiterRepository.findById(mitarbeiterId);
        if (optionalMitarbeiter.isPresent()){
            Mitarbeiter mitarbeiter = optionalMitarbeiter.get();
            mitarbeiter.setName(mitarbeiterDTO.getName());
            mitarbeiterRepository.save(mitarbeiter);
        }
    }
}
