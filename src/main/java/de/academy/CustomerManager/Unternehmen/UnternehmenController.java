package de.academy.CustomerManager.Unternehmen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UnternehmenController {

    private UnternehmenService unternehmenService;


    @Autowired
    public UnternehmenController(UnternehmenService unternehmenService) {
        this.unternehmenService = unternehmenService;
    }

    @GetMapping("/unternehmen")
    public String showUnternehmen(Model model){
        model.addAttribute("unternehmenListe", unternehmenService.getAllUnternehmen());
        model.addAttribute("unternehmenDTO", new UnternehmenDTO(""));
        return "unternehmen/UnternehmenIndex";
    }

    @PostMapping("/unternehmen")
    public String addUnternehmen(@ModelAttribute(value = "unternehmenDTO") UnternehmenDTO unternehmenDTO){
        unternehmenService.addUnternehmen(unternehmenDTO);
        return "redirect:/unternehmen";
    }


//    @GetMapping("/unternehmen/add")
//    public String addUnternehmen(Model model){
//        model.addAttribute("UnternehmenDTO", new UnternehmenDTO());
//
//        return "unternehmen/unternehmenadd";
//
//    }

    //getmapping unternehmen

    //getmapping unternehmen hinzufügen

    //getmapping edit

    //postmapping delite

    //postmapping submit
}
