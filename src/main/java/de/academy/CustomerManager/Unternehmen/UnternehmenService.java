package de.academy.CustomerManager.Unternehmen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnternehmenService {

    private UnternehmenRepository unternehmenRepository;

    @Autowired
    public UnternehmenService(UnternehmenRepository unternehmenRepository) {
        this.unternehmenRepository = unternehmenRepository;
    }

    public void addUnternehmen(UnternehmenDTO unternehmenDTO){
        Unternehmen unternehmen = new Unternehmen(unternehmenDTO.getName());
        unternehmenRepository.save(unternehmen);
    }

    public List<Unternehmen> getAllUnternehmen(){
        return unternehmenRepository.findAll();
    }

    public Unternehmen getById(int id){
        return unternehmenRepository.findById(id).orElse(null);
    }
}
