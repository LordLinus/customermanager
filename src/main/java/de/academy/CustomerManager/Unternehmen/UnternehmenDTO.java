package de.academy.CustomerManager.Unternehmen;

import javax.validation.constraints.NotEmpty;

public class UnternehmenDTO {
    @NotEmpty
    private String name;

    public UnternehmenDTO(@NotEmpty String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
