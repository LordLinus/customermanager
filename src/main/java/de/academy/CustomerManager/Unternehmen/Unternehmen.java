package de.academy.CustomerManager.Unternehmen;


import de.academy.CustomerManager.Mitarbeiter.Mitarbeiter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Unternehmen {

    @Id
    @GeneratedValue
    private int id;
    private String name;

    @OneToMany(mappedBy = "unternehmen")
    List<Mitarbeiter> mitarbeiterListe;

    public Unternehmen() {
    }
    public Unternehmen(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Mitarbeiter> getMitarbeiterListe() {
        return mitarbeiterListe;
    }
}
