package de.academy.CustomerManager.Unternehmen;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UnternehmenRepository extends CrudRepository<Unternehmen,Integer> {
    List<Unternehmen> findAll();

}
